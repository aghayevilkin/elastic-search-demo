package com.example.elasticsearchdemo.service;

import com.example.elasticsearchdemo.dto.ProductRequestDto;
import com.example.elasticsearchdemo.dto.ProductResponseDto;

public interface ProductService {
    void createProduct(ProductRequestDto productRequestDto);

    ProductResponseDto findById(Long id);
}
