package com.example.elasticsearchdemo.repository;

import com.example.elasticsearchdemo.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
